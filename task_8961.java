import java.util.Scanner;

public class task_8961 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] arr = new int[n];
        int index = 0, cost = 0;
        for (int i = 0; i < n; i++) {
            arr[i] = scan.nextInt();
        }
        int min = arr[0];
        for (int i = 0; i < n; i++) {
            if (arr[i] < min) {
                min = arr[i];
                index = i;
            }
        }
        cost = arr[0];
        arr[0] = min;
        arr[index] = cost;
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}

