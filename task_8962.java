import java.util.Scanner;

public class task_8962 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] arr = new int[n];
        int max = 0, index = 0, cost = 0;
        for (int i = 0; i < n; i++) {
            arr[i] = scan.nextInt();
        }
        for (int i = 0; i < n; i++) {
            if (arr[i] >= max) {
                max = arr[i];
                index = i;
            }
        }
        cost = arr[n-1];
        arr[n-1] = max;
        arr[index] = cost;
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }

    }
}

