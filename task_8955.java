import java.util.Scanner;

public class task_8955 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] arr = new int[n];
        int count = 0, count1 = n;
        for (int i = 0; i < n; i++) {
            arr[i] = scan.nextInt();
            if (arr[i] > 0) {
                count++;
                count1--;
            }
        }
        if (count1 != n) {
            System.out.println(count);
            for (int i = 0; i < n; i++) {
                if (arr[i] > 0) {
                    System.out.print(arr[i] + " ");
                }
            }
        } else {
            System.out.println("NO");
        }
    }
}
